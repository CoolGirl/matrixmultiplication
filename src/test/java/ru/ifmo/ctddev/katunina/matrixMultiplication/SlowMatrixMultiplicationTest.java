package ru.ifmo.ctddev.katunina.matrixMultiplication;

import org.junit.Assert;
import org.junit.Test;

public class SlowMatrixMultiplicationTest {

    @Test(expected = IllegalArgumentException.class)
    public void testValidationIsPerformed() {
        int [][] a = {{}};
        int [][] b = {{0}};
        SlowMatrixMultiplication.multiply(a, b);
    }

    @Test
    public void testMultiplicationIsCorrect() {
        int [][] a = {{1, 2}, {2, 3}, {3, 4}};
        int [][] b = {{1, 0}, {0, 1}};
        int [][] c = SlowMatrixMultiplication.multiply(a, b);
        for (int i = 0; i < a.length; i++)
            for (int j = 0; j < a[i].length; j++)
                if (a[i][j] != c[i][j]) {
                    Assert.fail();
                }
    }

    @Test
    public void testMultiplicationIsCorrect2() {
        int[][] a = {{1, 0, 2, -1}, {-2, 0, -4, 2},
                {1, 0, 2, -1}, {3, 0, 6, -3}};
        int[][] b = {{2, 1, 3, -1}, {-4, -2, -6, 2},
                {2, 1, 3, -1}, {6, 3, 9, -3}};
        int[][] result = SlowMatrixMultiplication.multiply(a, b);
        for (int[] aResult : result) {
            for (int j = 0; j < result[0].length; j++) {
                if (aResult[j] != 0) {
                    Assert.fail();
                }
            }
        }
    }
}
