package ru.ifmo.ctddev.katunina.matrixMultiplication;

import org.junit.Assert;
import org.junit.Test;

public class StrassenMatrixMultiplicationTest {

    @Test(expected = IllegalArgumentException.class)
    public void testMatrixValidationIsPerformed() {
        int [][] a = {{}};
        int [][] b = {{1}};
        StrassenMatrixMultiplication.multiply(a, b);
    }

    @Test
    public void testSmallMatrixMultiplication() {
        int [][] a = {{1, 2}, {2, 3}, {3, 4}};
        int [][] b = {{1, 0}, {0, 1}};
        int [][] c = StrassenMatrixMultiplication.multiply(a, b);
        for (int i = 0; i < a.length; i++)
            for (int j = 0; j < a[i].length; j++)
                if (a[i][j] != c[i][j]) {
                    Assert.fail();
                }
    }

    @Test
    public void testBigMatrixMultiplication() {
        int[][] a = new int [129][129];
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[0].length; j++) {
                a[i][j] = 1;
            }
        }
        int[][] transposedMethodResult = TransposedMatrixMultiplication.multiply(a, a);
        int[][] strassenMethodResult = StrassenMatrixMultiplication.multiply(a, a);
        checkMatricesAreEqual(transposedMethodResult, strassenMethodResult);
    }

    private void checkMatricesAreEqual(int[][] a, int[][] b) {
        if (a.length != b.length || b[0].length != a[0].length) {
            Assert.fail();
        }
        for (int i = 0; i < a.length; i++)
            for (int j = 0; j < a[i].length; j++)
                if (a[i][j] != b[i][j]) {
                    Assert.fail();
                }
    }

    @Test
    public void testMultiplicationIsCorrect2() {
        int[][] a = {{1, 0, 2, -1}, {-2, 0, -4, 2},
                {1, 0, 2, -1}, {3, 0, 6, -3}};
        int[][] b = {{2, 1, 3, -1}, {-4, -2, -6, 2},
                {2, 1, 3, -1}, {6, 3, 9, -3}};
        int[][] result = StrassenMatrixMultiplication.multiply(a, b);
        for (int[] aResult : result) {
            for (int j = 0; j < result[0].length; j++) {
                if (aResult[j] != 0) {
                    Assert.fail();
                }
            }
        }
    }
}
