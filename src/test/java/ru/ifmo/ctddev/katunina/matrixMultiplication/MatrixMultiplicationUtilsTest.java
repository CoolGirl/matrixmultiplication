package ru.ifmo.ctddev.katunina.matrixMultiplication;

import org.junit.Assert;
import org.junit.Test;

public class MatrixMultiplicationUtilsTest {

    @Test
    public void testValidateZeroDimensionMatrix() {
        int [][] a = {{0}};
        int [][] b = {{}};
        Assert.assertEquals(MatrixMultiplicationUtils.validateMatrices(a, b),
                MatrixMultiplicationUtils.ZERO_DIMENSION_ERROR_MSG);
    }

    @Test
    public void testValidateFirstMatrixIsNotValid () {
        int [][] a = {{0}, {1, 0}};
        int [][] b = {{1}};
        Assert.assertEquals(MatrixMultiplicationUtils.validateMatrices(a, b),
                String.format(MatrixMultiplicationUtils.INVALID_FIRST_MATRIX_MSG, 2, 2, 1));
    }

    @Test public void testValidateSecondMatrixIsNotValid () {
        int [][] a = {{0, 1}, {1, 1}};
        int [][] b = {{0, 0}, {1}};
        Assert.assertEquals(MatrixMultiplicationUtils.validateMatrices(a, b),
                String.format(MatrixMultiplicationUtils.INVALID_SECOND_MATRIX_MSG, 2, 1, 2));
    }

    @Test public void testValidateInconsistentDimensions() {
        int [][] a = {{0}};
        int [][] b = {{2}, {1}};
        Assert.assertEquals(MatrixMultiplicationUtils.validateMatrices(a,b),
                String.format(MatrixMultiplicationUtils.INCONSISTENT_DIMENSIONS, 1, 1, 2, 1));
    }

    @Test
    public void testValidateValidMatrices() {
        int [][] a = {{0}, {1}};
        int [][] b = {{1, 2, 3, 4}};
        Assert.assertEquals(MatrixMultiplicationUtils.validateMatrices(a, b), "");
    }

    @Test
    public void testTransposeMatrix() {
        int [][] a = {{1, 2, 3, 4}, {5, 6, 7, 8}};
        int [][] aTransposed = {{1, 5}, {2, 6}, {3, 7}, {4, 8}};
        int [][] result = MatrixMultiplicationUtils.transposeMatrix(a);
        checkMatricesAreEqual(aTransposed, result);
    }

    private void checkMatricesAreEqual(int[][] a, int[][] b) {
        if (a.length != b.length || b[0].length != a[0].length) {
            Assert.fail();
        }
        for (int i = 0; i < a.length; i++)
            for (int j = 0; j < a[i].length; j++)
                if (a[i][j] != b[i][j]) {
                    Assert.fail();
                }
    }

    @Test
    public void testSummationMatrices() {
        int [][] a = {{0, 1}, {1, 1}};
        int [][] b = {{2, 3}, {4, 5}};
        int [][] sum = {{2, 4}, {5, 6}};
        int [][] result = MatrixMultiplicationUtils.sum(a, b);
        checkMatricesAreEqual(sum, result);
    }

    @Test
    public void testSubtractionMatrices() {
        int [][] a = {{0, 1}, {1, 1}};
        int [][] b = {{2, 3}, {4, 5}};
        int [][] sum = {{-2, -2}, {-3, -4}};
        int [][] result = MatrixMultiplicationUtils.subtract(a, b);
        checkMatricesAreEqual(sum, result);
    }

    @Test
    public void testComplement() {
        int [][] a = {{1, 4}, {2, 5}, {3, 6}};
        int [][] aComplemented = {{1, 4, 0, 0}, {2, 5, 0, 0}, {3, 6, 0, 0}, {0, 0, 0, 0}};
        int [][] result = MatrixMultiplicationUtils.complement2SquarePowerOfTwo(a, 2, 2);
        checkMatricesAreEqual(aComplemented, result);
    }

    @Test
    public void testComplement2AnotherMatrixDimension() {
        int [][] a = {{1, 4}, {2, 5}};
        int [][] aComplemented = {{1, 4, 0, 0}, {2, 5, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}};
        int [][] result = MatrixMultiplicationUtils.complement2SquarePowerOfTwo(a, 4, 4);
        checkMatricesAreEqual(aComplemented, result);
    }

    @Test
    public void testSplitMatrix() {
        int [][] a = {{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}};
        int [][] a11 = {{1, 2}, {5, 6}};
        int [][] a12 = {{3, 4}, {7, 8}};
        int [][] a21 = {{9, 10}, {13, 14}};
        int [][] a22 = {{11, 12}, {15, 16}};
        int [][] a11Result = new int[2][2];
        int [][] a12Result = new int[2][2];
        int [][] a21Result = new int[2][2];
        int [][] a22Result = new int[2][2];
        MatrixMultiplicationUtils.splitMatrix(a, a11Result, a12Result, a21Result, a22Result);
        checkMatricesAreEqual(a11, a11Result);
        checkMatricesAreEqual(a12, a12Result);
        checkMatricesAreEqual(a21, a21Result);
        checkMatricesAreEqual(a22, a22Result);
    }

    @Test
    public void testCollectMatrix() {
        int [][] a = {{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}};
        int [][] a11 = {{1, 2}, {5, 6}};
        int [][] a12 = {{3, 4}, {7, 8}};
        int [][] a21 = {{9, 10}, {13, 14}};
        int [][] a22 = {{11, 12}, {15, 16}};
        int [][] aResult = new int[4][4];
        MatrixMultiplicationUtils.collectMatrix(aResult, a11, a12, a21, a22);
        checkMatricesAreEqual(a, aResult);
    }
}
