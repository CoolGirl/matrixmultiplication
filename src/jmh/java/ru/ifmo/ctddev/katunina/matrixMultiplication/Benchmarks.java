package ru.ifmo.ctddev.katunina.matrixMultiplication;

import org.openjdk.jmh.annotations.*;

import java.util.Random;

public class Benchmarks {


    @Benchmark
    public void testSlowMultiplication(MyState state) {
        SlowMatrixMultiplication.multiply(state.a, state.b);
    }

    @Benchmark
    public void testTransposedMultiplication(MyState state) {
        TransposedMatrixMultiplication.multiply(state.a, state.b);
    }

    @Benchmark
    public void testStrassen(MyState state) {
        StrassenMatrixMultiplication.multiply(state.a, state.b);
    }

    @Benchmark
    public void testStrassenParallel(MyState state) {
        StrassenParallel.multiply(state.a, state.b);
    }
}
