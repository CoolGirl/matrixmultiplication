package ru.ifmo.ctddev.katunina.matrixMultiplication;

import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;

import java.util.Random;

@State(Scope.Thread)
public class MyState {
    private Random r = new Random();

    private int[][] generateRandomSquareMatrix(int dimension) {
        int[][] result = new int[dimension][dimension];
        for (int i = 0; i < dimension; i++) {
            for (int j = 0; j < dimension; j++) {
                result[i][j] = r.nextInt();
            }
        }
        return result;
    }

    @Param({//"100", "200", "400"
             "600", "800", "1000",
            "1200", "1400"})
            //"1600", "1800", "2000",
            // "2200", "2400", "2600", "2800", "3000"})
    int n;

    @Setup
    public void doSetup() {
        a = generateRandomSquareMatrix(n);
        b = generateRandomSquareMatrix(n);
    }

    int[][] a;
    int[][] b;
}
