package ru.ifmo.ctddev.katunina.matrixMultiplication;

class MatrixMultiplicationUtils {

    static final String ZERO_DIMENSION_ERROR_MSG = "Zero dimension matrices multiplication" +
            " is not supported.";
    static final String INVALID_FIRST_MATRIX_MSG = "Row %s of first matrix has %s columns while " +
            "previous ones have %s columns.";
    static final String INVALID_SECOND_MATRIX_MSG = "Row %s of second matrix has %s columns while " +
            "previous ones have %s columns.";
    static final String INCONSISTENT_DIMENSIONS = "%sx%s matrix can't be multiplied by %sx%s matrix";

    static String validateMatrices(int[][] a, int[][] b) {
        int aRows = a.length;
        int bRows = b.length;
        if (aRows == 0 || bRows == 0) {
            return ZERO_DIMENSION_ERROR_MSG;
        }
        int aColumns = a[0].length;
        if (aColumns == 0) {
            return ZERO_DIMENSION_ERROR_MSG;
        }
        for (int i = 1; i < aRows; i++) {
            int rowColumns = a[i].length;
            if (rowColumns != aColumns) {
                return String.format(INVALID_FIRST_MATRIX_MSG, i + 1, rowColumns, aColumns);
            }
        }
        int bColumns = b[0].length;
        if (bColumns == 0) {
            return ZERO_DIMENSION_ERROR_MSG;
        }
        if (aColumns != bRows) {
            return String.format(INCONSISTENT_DIMENSIONS, aRows,
                    aColumns, bRows, bColumns);
        }
        for (int i = 1; i < bRows; i++) {
            int rowColumns = b[i].length;
            if (rowColumns != bColumns) {
                return String.format(INVALID_SECOND_MATRIX_MSG, i + 1, rowColumns, bColumns);
            }
        }
        return "";
    }

    static int[][] transposeMatrix(int [][] a) {
        int [][] result = new int[a[0].length][a.length];
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[0].length; j++) {
                result[j][i] = a[i][j];
            }
        }
        return result;
    }

    static int[][] sum(int[][] a, int[][] b) {
        int[][] c = new int[a.length][a[0].length];
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                c[i][j] = a[i][j] + b[i][j];
            }
        }
        return c;
    }

    static int[][] subtract(int[][] a, int[][] b) {
        int[][] c = new int[a.length][a[0].length];
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                c[i][j] = a[i][j] - b[i][j];
            }
        }
        return c;
    }

    static int[][] complement2SquarePowerOfTwo(int[][] a, int n, int m) {
        int maxDimension = Math.max(Math.max(Math.max(a.length, a[0].length), n), m);
        int dim = 1;
        while (maxDimension > dim) {
            dim <<= 1;
        }
        int[][] aComplemented = new int[dim][dim];
        for (int i = 0; i < a.length; i++)
            System.arraycopy(a[i], 0, aComplemented[i], 0, a[0].length);
        return aComplemented;
    }

    static void splitMatrix(int[][] a, int[][] a11, int[][] a12, int[][] a21, int[][] a22) {
        int blockLength = a.length / 2;
        for (int i = 0; i < blockLength; i++) {
            System.arraycopy(a[i], 0, a11[i], 0, blockLength);
            System.arraycopy(a[i + blockLength],0, a21[i], 0, blockLength);
            System.arraycopy(a[i], blockLength, a12[i], 0, blockLength);
            System.arraycopy(a[i + blockLength], blockLength, a22[i], 0, blockLength);
        }
    }

    static void collectMatrix(int[][] a, int[][] a11, int[][] a12, int[][] a21, int[][] a22) {
        int blockLength = a11.length;
        for (int i = 0; i < blockLength; i++) {
            System.arraycopy(a11[i], 0, a[i], 0, blockLength);
            System.arraycopy(a12[i], 0, a[i], blockLength, blockLength);
            System.arraycopy(a21[i], 0, a[blockLength + i], 0, blockLength);
            System.arraycopy(a22[i], 0, a[blockLength + i], blockLength, blockLength);
        }
    }
}
