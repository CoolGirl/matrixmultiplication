package ru.ifmo.ctddev.katunina.matrixMultiplication;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

class StrassenParallel {

    private static final int SMALL_DIMENSION_BOUNDARY = 128;

    static int[][] multiply(int[][] a, int[][] b) {
        String validationResult = MatrixMultiplicationUtils.validateMatrices(a, b);
        if (!validationResult.isEmpty()) {
            throw new IllegalArgumentException(validationResult);
        }
        int aRows = a.length;
        int aColumns = a[0].length;
        int bRows = b.length;
        int bColumns = b[0].length;
        int[][] aComplemented = MatrixMultiplicationUtils.complement2SquarePowerOfTwo(a, bRows, bColumns);
        int[][] bComplemented = MatrixMultiplicationUtils.complement2SquarePowerOfTwo(b, aRows, aColumns);
        Task task = new Task(aComplemented, bComplemented, aComplemented.length);
        ForkJoinPool pool = new ForkJoinPool();
        int[][] resultComplemented = pool.invoke(task);
        int[][] result = new int[aRows][bColumns];
        for (int i = 0; i < aRows; i++) {
            System.arraycopy(resultComplemented[i], 0, result[i], 0, bColumns);
        }
        return result;
    }

    private static class Task extends RecursiveTask<int[][]> {
        int n;
        int[][]a;
        int[][]b;

        Task(int[][] a, int[][] b, int n) {
            this.a = a;
            this.b = b;
            this.n = n;
        }

        @Override
        protected int[][] compute() {
            if (a.length <= SMALL_DIMENSION_BOUNDARY &&
                    a[0].length <= SMALL_DIMENSION_BOUNDARY &&
                    b.length <= SMALL_DIMENSION_BOUNDARY
                    && b[0].length <= SMALL_DIMENSION_BOUNDARY) {
                return TransposedMatrixMultiplication.multiply(a, b);
            }
            int blockSize = n >> 1;
            int[][] a11 = new int[blockSize][blockSize];
            int[][] a12 = new int[blockSize][blockSize];
            int[][] a21 = new int[blockSize][blockSize];
            int[][] a22 = new int[blockSize][blockSize];
            int[][] b11 = new int[blockSize][blockSize];
            int[][] b12 = new int[blockSize][blockSize];
            int[][] b21 = new int[blockSize][blockSize];
            int[][] b22 = new int[blockSize][blockSize];

            MatrixMultiplicationUtils.splitMatrix(a, a11, a12, a21, a22);
            MatrixMultiplicationUtils.splitMatrix(b, b11, b12, b21, b22);

            Task task1 = new Task(MatrixMultiplicationUtils.subtract(a12, a22), MatrixMultiplicationUtils.sum(b21, b22), blockSize);
            Task task2 = new Task(MatrixMultiplicationUtils.sum(a11, a22), MatrixMultiplicationUtils.sum(b11, b22), blockSize);
            Task task3 = new Task(MatrixMultiplicationUtils.subtract(a11, a21), MatrixMultiplicationUtils.sum(b11, b12), blockSize);
            Task task4 = new Task(MatrixMultiplicationUtils.sum(a11, a12), b22, blockSize);
            Task task5 = new Task(a11, MatrixMultiplicationUtils.subtract(b12, b22), blockSize);
            Task task6 = new Task(a22, MatrixMultiplicationUtils.subtract(b21, b11), blockSize);
            Task task7 = new Task(MatrixMultiplicationUtils.sum(a21, a22), b11, blockSize);

            task1.fork();
            task2.fork();
            task3.fork();
            task4.fork();
            task5.fork();
            task6.fork();
            task7.fork();

            int[][] p1 = task1.join();
            int[][] p2 = task2.join();
            int[][] p3 = task3.join();
            int[][] p4 = task4.join();
            int[][] p5 = task5.join();
            int[][] p6 = task6.join();
            int[][] p7 = task7.join();

            int[][] c11 = MatrixMultiplicationUtils.sum(MatrixMultiplicationUtils.subtract(MatrixMultiplicationUtils.sum(p1, p2), p4), p6);
            int[][] c12 = MatrixMultiplicationUtils.sum(p4, p5);
            int[][] c21 = MatrixMultiplicationUtils.sum(p6, p7);
            int[][] c22 = MatrixMultiplicationUtils.subtract(MatrixMultiplicationUtils.sum(MatrixMultiplicationUtils.subtract(p2, p3), p5), p7);

            int[][] result = new int[n][n];
            MatrixMultiplicationUtils.collectMatrix(result, c11, c12, c21, c22);
            return result;
        }
    }
}
