package ru.ifmo.ctddev.katunina.matrixMultiplication;

class StrassenMatrixMultiplication {

    private static final int SMALL_DIMENSION_BOUNDARY = 128;

    static int[][] multiply(int[][] a, int[][]b) {
        String validationResult = MatrixMultiplicationUtils.validateMatrices(a, b);
        if (!validationResult.isEmpty()) {
            throw new IllegalArgumentException(validationResult);
        }
        int aRows = a.length;
        int aColumns = a[0].length;
        int bRows = b.length;
        int bColumns = b[0].length;
        int[][] aComplemented = MatrixMultiplicationUtils.complement2SquarePowerOfTwo(a, bRows, bColumns);
        int[][] bComplemented = MatrixMultiplicationUtils.complement2SquarePowerOfTwo(b, aRows, aColumns);
        int[][] resultComplemented = multiply(aComplemented, bComplemented, aComplemented.length);
        int[][] result = new int[aRows][bColumns];
        for (int i = 0; i < aRows; i++) {
            System.arraycopy(resultComplemented[i], 0, result[i], 0, bColumns);
        }
        return result;
    }

    private static int[][] multiply(int[][] a, int[][] b, int n) {
        if (a.length <= SMALL_DIMENSION_BOUNDARY &&
                a[0].length <= SMALL_DIMENSION_BOUNDARY &&
                b.length <= SMALL_DIMENSION_BOUNDARY
                && b[0].length <= SMALL_DIMENSION_BOUNDARY) {
            return TransposedMatrixMultiplication.multiply(a, b);
        }
        int blockSize = n >> 1;
        int[][] a11 = new int[blockSize][blockSize];
        int[][] a12 = new int[blockSize][blockSize];
        int[][] a21 = new int[blockSize][blockSize];
        int[][] a22 = new int[blockSize][blockSize];
        int[][] b11 = new int[blockSize][blockSize];
        int[][] b12 = new int[blockSize][blockSize];
        int[][] b21 = new int[blockSize][blockSize];
        int[][] b22 = new int[blockSize][blockSize];

        MatrixMultiplicationUtils.splitMatrix(a, a11, a12, a21, a22);
        MatrixMultiplicationUtils.splitMatrix(b, b11, b12, b21, b22);

        int[][] p1 = multiply(MatrixMultiplicationUtils.subtract(a12, a22), MatrixMultiplicationUtils.sum(b21, b22), blockSize);
        int[][] p2 = multiply(MatrixMultiplicationUtils.sum(a11, a22), MatrixMultiplicationUtils.sum(b11, b22), blockSize);
        int[][] p3 = multiply(MatrixMultiplicationUtils.subtract(a11, a21), MatrixMultiplicationUtils.sum(b11, b12), blockSize);
        int[][] p4 = multiply(MatrixMultiplicationUtils.sum(a11, a12), b22, blockSize);
        int[][] p5 = multiply(a11, MatrixMultiplicationUtils.subtract(b12, b22), blockSize);
        int[][] p6 = multiply(a22, MatrixMultiplicationUtils.subtract(b21, b11), blockSize);
        int[][] p7 = multiply(MatrixMultiplicationUtils.sum(a21, a22), b11, blockSize);

        int[][] c11 = MatrixMultiplicationUtils.sum(MatrixMultiplicationUtils.subtract(MatrixMultiplicationUtils.sum(p1, p2), p4), p6);
        int[][] c12 = MatrixMultiplicationUtils.sum(p4, p5);
        int[][] c21 = MatrixMultiplicationUtils.sum(p6, p7);
        int[][] c22 = MatrixMultiplicationUtils.subtract(MatrixMultiplicationUtils.sum(MatrixMultiplicationUtils.subtract(p2, p3), p5), p7);

        int[][] result = new int[n][n];
        MatrixMultiplicationUtils.collectMatrix(result, c11, c12, c21, c22);
        return result;
    }
}
