package ru.ifmo.ctddev.katunina.matrixMultiplication;

class TransposedMatrixMultiplication {

    static int[][] multiply(int[][] a, int[][] b) {
        String validationResult = MatrixMultiplicationUtils.validateMatrices(a, b);
        if (!validationResult.isEmpty()) {
            throw new IllegalArgumentException(validationResult);
        }
        int aRows = a.length;
        int aColumns = a[0].length;
        int [][] bTransposed = MatrixMultiplicationUtils.transposeMatrix(b);
        int bRows = bTransposed[0].length;
        int bColumns = bTransposed.length;
        int[][] c = new int[aRows][bColumns];
        for (int i = 0; i < aRows; i++)
            for (int k = 0; k < bColumns; k++) {
                int sum = 0;
                for (int j = 0; j < bRows; j++)
                    sum += a[i][j] * bTransposed[k][j];
                c[i][k] = sum;
            }
        return c;
    }
}
